<?php 
session_start(); 
if (!$_SESSION['user']) {header('Location: /login.php');}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/page.css">
  <!--<link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker.css">-->
  <link href="css/datepicker.css" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>СКиУЗ</title>
</head>
<body>
  <!---------------------NavBar---------------------->
<?php 
include 'include/head.html';
?>


<!---------------------IndexPage---------------------->
  
        <?php //error_reporting(0); 
          require_once 'include/db.php';
          $dbhost = $GLOBALS['dbhost'];
          $dbuser = $GLOBALS['dbuser'];
          $dbpass = $GLOBALS['dbpass'];
          $dbname = $GLOBALS['dbname'];
          $mysql = mysqli_connect("$dbhost","$dbuser","$dbpass","$dbname");

          if (!$mysql) {echo '<div class="alert alert-danger container" role="alert">Отсутствуеь подключение к базе данных<br>'. mysqli_connect_error() .PHP_EOL.'</div>';exit();}
          $mysql->set_charset("utf8");

          $t = time();
          $i = 0;
          $j = 0;
          $k = 0;
          $s = mysqli_query ($mysql, "SELECT * FROM `applications` WHERE `status_appl` = 1");
          while ($result = mysqli_fetch_assoc($s)){
            if ($result['end_t']<$t){$i++;}
            if ($result['start_t']>$t){$j++;}
            if (($result['end_t']>$t) && ($result['start_t']<$t)){$k++;}}?>
                              <div class="container mt-3">
                                <div class="row mb-3">
                                  <div class="col-sm">
                                    <div class="card">
                                      <div class="card-body p-0 d-flex align-items-center">
                                        <img src="img/sharp_offline_pin_white_36dp.png" class="bg-success mr-3" width="45" height="45"></img>
                                          <div class="text-muted text-uppercase">активные заявки - <?= $k ?></div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm">
                                    <div class="card">
                                      <div class="card-body p-0 d-flex align-items-center">
                                        <img src="img/sharp_warning_white_36dp.png" class="bg-warning mr-3" width="45" height="45"></img>
                                          <div class="text-muted text-uppercase">неактивные заявки - <?= $j ?></div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm">
                                    <div class="card">
                                      <div class="card-body p-0 d-flex align-items-center">
                                        <img src="img/sharp_cancel_white_36dp.png" class="bg-danger mr-3" width="45" height="45"></img>
                                          <div class="text-muted text-uppercase">оконченные заявки - <?= $i ?></div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
          <?php
          if ($sql = mysqli_query ($mysql, "SELECT * FROM `applications` ORDER BY `id` desc")){
              while ($result = mysqli_fetch_array($sql)) {
                $date_now = time();
                  if ($result['end_t'] < $date_now){$date_class = 'bg-danger text-white';}
                  elseif (($result['end_t'] > $date_now) & ($result['start_t']) < $date_now) $date_class='bg-white text-dark';
                  elseif ($result['start_t'] > $date_now) $date_class='bg-warning text-dark';
                    if ($result['status_appl'] == 1){
                      // ------------------------ Request ----------------------
                        
                         echo '<!-------applications------->
                        <div class="container-fluid">
                              <div class="accordion" id="accordionExample'.$result['id'].'">
                              <div class="card "> 
                        <div class="card-head" id="heading'.$result['id'].'">
                                <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse'.$result['id'].'" aria-expanded="true"  style="width:100%">
                                  <table class="table table-hover table-bordered text-left '.$date_class.'" >
                                    <tr>';
                      if ($_SESSION['user_id']==1){echo '<td>id'.$result['id'].'</td>';}
                                echo '<td>Заявка № '.$result['app'].' / '.$result['app_old'].'</td>
                                      <td>Описание: '.$result['op_guid'].'</td>
                                      <td>Просимое время с '.date('d.m.Y H:i',$result['start_t']).'</td>
                                      <td>до '.date('d.m.Y H:i',$result['end_t']).'</td>
                                      <td>'.$result['fullname'].'</td>
                                    </tr>           
                                  </table>
                                </button>
                              </div>
                              <!---------------------   Body Request   ------------------>
                              <div id="collapse'.$result['id'].'" class="collapsing" aria-labelledby="heading'.$result['id'].'" data-parent="#accordionExample'.$result['id'].'">
                                <div class="card-body">
                                  <table class="table table-bordered">
                                    <tbody>
                                      <tr>
                                        <td>Заявка № '.$result['app'].' / '.$result['app_old'].'</td>
                                        <td>Предприятие: '.$result['ent'].'</td>
                                        <td>Категория: '.$result['cat'].'</td>
                                        <td>Просимое время: c '.date('d.m.Y H:i',$result['start_t']).'</td>
                                        <td>до '.date('d.m.Y H:i',$result['end_t']).'</td>
                                        <td>Заявку составил: '.$result['fullname'].'</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">Содержание работ: '.$result['job_cont'].'</td>
                                        <td colspan="2">Оперативные указания: '.$result['op_guid'].'</td>
                                        <td colspan="">Остаются в работе: '.$result['rem_work'].'</td>
                                        <td colspan="">Выводятся из работы'.$result['der_work'].'</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <div class="container text-center" style="margin:10px auto 10px auto;">';
                                  if ($_SESSION['user_id']==1){echo ' <button class="btn btn-danger" href="include/edit_request.php?id='.$result['id'].'" data-toggle="modal" data-target="#EditexampleModal'.$result['id'].'">Редактировать</button> ';}
                                    echo '<button class="btn btn-success" href="include/ext_request.php?id='.$result['id'].'" data-toggle="modal" data-target="#exampleModal'.$result['id'].'">Продлить</button> 
                                    <button class="btn btn-danger" ><a href="include/closed_request.php?id='.$result['id'].'" style="color:#fff">Закрыть</a></button>
                                      <form action="include/ext_request.php" method="get">
                                        <div class="modal fade text-left" id="exampleModal'.$result['id'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static">
                                          <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Продлить заявку</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                                <label >ID заявки</label>
                                                <input type="text" class="form-control" name="id" id="id" placeholder="" value="'.$result['id'].'" readonly>
                                                <label> Новый № Заявки</label>
                                                <input type="text" class="form-control" name="app" id="app" placeholder="" required></input>
                                                <label >№ Заявки</label>
                                                <input type="text" class="form-control" name="app_old" id="app_old" placeholder="" value="'.$result['app'].'"" readonly></input>
                                                <label >Предприятие:</label>
                                                <input type="text" class="form-control" name="ent" id="ent" placeholder="" value="'.$result['ent'].'" readonly>
                                                <label >Категория:</label>
                                                <input type="text" class="form-control" name="cat" id="cat" placeholder="" value="'.$result['cat'].'" readonly>
                                                <label >Содержание работ:</label>
                                                <input type="text" class="form-control" name="job_cont" id="job_cont" placeholder="" value="'.$result['job_cont'].'"; readonly>
                                                <label >Оперативные указания:</label>
                                                <input type="text" class="form-control" name="op_guid" id="op_guid" placeholder="" value="'.$result['op_guid'].'"; readonly>
                                                <label >Остаются в работе:</label>
                                                <input type="text" class="form-control" name="rem_work" id="rem_work" placeholder="" value="'.$result['rem_work'].'"; readonly>
                                                <label >Выводятся из работы:</label>
                                                <input type="text" class="form-control" name="der_work" id="der_work" placeholder="" value="'.$result['der_work'].'"; readonly>
                                                <label >Дата начала</label>
                                                <input type="text" name="start_t" class="datepicker-here form-control"  data-time-format=\'hh:ii\' data-position=\'top left\' placeholder="'.date('d.m.Y H:i',$result['start_t']).'" required></input>
                                                <label >Дата окончания</label>
                                                <input type="text" name="end_t" class="datepicker-here form-control"  data-time-format=\'hh:ii\' data-position=\'top left\' placeholder="'.date('d.m.Y H:i',$result['end_t']).'" required></input>
                                                <label >Заявку продлил:</label>
                                                <input type="text" class="form-control" name="fullname" id="fullname" placeholder="" value="'.$result['fullname'].'"; readonly>
                                                  <div class="modal-footer">
                                                    <button class="btn btn-success" type="submit" >Продлить</button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Выход</button>
                                                  </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </form>
                                      <form action="include/edit_request.php" method="get">
                                        <div class="modal fade text-left" id="EditexampleModal'.$result['id'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static">
                                          <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="EditexampleModalLongTitle">Продлить заявку</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                                <label >ID заявки</label>
                                                <input type="text" class="form-control" name="id" id="id" placeholder="" value="'.$result['id'].'" readonly>
                                                <label>№ Заявки</label>
                                                <input type="text" class="form-control" name="app" id="app" placeholder="" value="'.$result['app'].'"></input>
                                                <label>№ Старой заявки</label>
                                                <input type="text" class="form-control" name="app_old" id="app_old" placeholder="" value="'.$result['app_old'].'"></input>
                                                <label >Предприятие:</label>
                                                <select class="custom-select mb-3" id="ent" name="ent" onchange="ent();">
                                                  <option  value="Ставропольэнерго">Ставропольэнерго</option>
                                                  <option  value="ЗЭС">ЗЭС</option>
                                                  <option  value="ВЭС">ВЭС</option>
                                                  <option  value="НЭС">НЭС</option>
                                                  <option  value="ПЭС">ПЭС</option>
                                                  <option  value="ЦЭС">ЦЭС</option>
                                                  <option  value="ЗЭС">СЭС</option>
                                                </select>
                                                <label >Категория</label>
                                                <select class="custom-select mb-3" id="cat" name="cat" onchange="cat();">
                                                  <option  value="НПЛ">НПЛ</option>
                                                  <option  value="ПЛ">ПЛ</option>
                                                  <option  value="АВ">АВ</option>
                                                  <option  value="НО">НО</option>
                                                </select>
                                                <label >Содержание работ:</label>
                                                <input type="text" class="form-control" name="job_cont" id="job_cont" placeholder="" value="'.$result['job_cont'].'";>
                                                <label >Оперативные указания:</label>
                                                <input type="text" class="form-control" name="op_guid" id="op_guid" placeholder="" value="'.$result['op_guid'].'"; >
                                                <label >Остаются в работе:</label>
                                                <input type="text" class="form-control" name="rem_work" id="rem_work" placeholder="" value="'.$result['rem_work'].'";>
                                                <label >Выводятся из работы:</label>
                                                <input type="text" class="form-control" name="der_work" id="der_work" placeholder="" value="'.$result['der_work'].'";>
                                                <label >Дата начала</label>
                                                <input type="text" name="start_t" class="datepicker-here form-control"  data-time-format=\'hh:ii\' data-position=\'top left\' placeholder="'.date('d.m.Y H:i',$result['start_t']).'" value="'.$result['start_t'].'" required></input>
                                                <label >Дата окончания</label>
                                                <input type="text" name="end_t" class="datepicker-here form-control"  data-time-format=\'hh:ii\' data-position=\'top left\' placeholder="'.date('d.m.Y H:i',$result['end_t']).'" required></input>
                                                <label >Статус заявки:</label>
                                                <input type="text" class="form-control" name="status_appl" id="status_appl" placeholder="" value="'.$result['status_appl'].'";>
                                                <label >Заявку создал (login):</label>
                                                <input type="text" class="form-control" name="respon" id="respon" placeholder="" value="'.$result['respon'].'";>
                                                <label >Заявку создал:</label>
                                                <input type="text" class="form-control" name="fullname" id="fullname" placeholder="" value="'.$result['fullname'].'";>
                                                  <div class="modal-footer">
                                                    <button class="btn btn-success" type="submit" >Сохранить</button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Выход</button>
                                                  </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </form>
                                      <!------------------------->
                                  </div>
                                </div>
                              </div>
                              </div><!--card-->
                              </div><!--accordion-->
                              </div><!--container-fluid-->';
              }
            }
          }elseif (isset($sql)){echo '<div class="alert alert-danger container" role="alert">Отсутствует база данных / указана неверно</div>';}
          $mysql->close();?>
   
<!-----------------Создать заявку---------------------->
<form action="include/add_request.php" method="post">
  <div class="container text-center">  
    <button type="button" class="btn btn-primary mt-3 mb-3" data-toggle="modal" data-target="#exampleModalLong">Создать заявку</button>
    <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Создать заявку</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-left">
            <label >№ Заявки</label>
            <input type="text" class="form-control" name="app" id="app" placeholder="" required>
            <label >Предприятие</label>
            <select class="custom-select mb-3" id="ent" name="ent" onchange="ent();">
              <option  value="Ставропольэнерго">Ставропольэнерго</option>
              <option  value="ЗЭС">ЗЭС</option>
              <option  value="ВЭС">ВЭС</option>
              <option  value="НЭС">НЭС</option>
              <option  value="ПЭС">ПЭС</option>
              <option  value="ЦЭС">ЦЭС</option>
              <option  value="ЗЭС">СЭС</option>
            </select>
            <label >Категория</label>
            <select class="custom-select mb-3" id="cat" name="cat" onchange="cat();">
              <option  value="НПЛ">НПЛ</option>
              <option  value="ПЛ">ПЛ</option>
              <option  value="АВ">АВ</option>
              <option  value="НО">НО</option>
            </select>
            <label >Содержание работ</label>
            <textarea  type="text" class="form-control" rows="3" name="job_cont" id="job_cont" placeholder="" required></textarea>
            <label >Оперативные указания</label>
            <textarea  type="text" class="form-control" rows="3" name="op_guid" id="op_guid" placeholder="" required></textarea>
            <label >Остаются в работе</label>
            <textarea  type="text" class="form-control" rows="3" name="rem_work" id="rem_work" placeholder="" required></textarea>
            <label >Выводятся из работы</label>
            <textarea  type="text" class="form-control" rows="3" name="der_work" id="der_work" placeholder="" required></textarea>
            <label >Дата начала</label>
            <input type="text" name="start_t" class="datepicker-here form-control"  data-time-format='hh:ii' data-position='top left' required></input>
            <label >Дата окончания</label>
            <input type="text" name="end_t" class="datepicker-here form-control"  data-time-format='hh:ii' data-position='top left' required></input>
            <label >Заявку составил</label>
            <input type="text" class="form-control" name="<?php echo $_SESSION['user_login'] ?>" id="<?php echo $_SESSION['user_name'] ?>" placeholder="<?php echo $_SESSION['user_name'] ?>" disabled>
            <div class="modal-footer">
              <button class="btn btn-success" type="submit">Создать</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Выход</button>
            </div>
          </div>
      </div>
    </div>
  </div>
</form>
<!-----------------script---------------------->
<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js" ></script>
<script src="js/bootstrap.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<!--<script src="js/bootstrap-datepicker.ru.min.js"></script>-->
<script src="js/datepicker.js"></script>
<script>
  // Выбор сегодняшнего дня
$('.datepicker-here').datepicker({
    todayButton: new Date(),
    minutesStep: 10,
    timepicker: true

})

</script>

</body>
</html>