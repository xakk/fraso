<?php 
session_start(); 
if ($_SESSION['user_id'] != '1'){
  header('Location: /index.php');
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/page.css">
  <link rel="stylesheet" type="text/css" href="css/regular.css">
  <!--<link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker.css">-->
  <link href="css/datepicker.css" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>СКиУЗ</title>
</head>
<body>
  <!---------------------NavBar---------------------->
<?php 
include 'include/head.html';
?>
<!---------------------IndexPage---------------------->
<div class="mb-2"></div>
          <?php error_reporting(0); 
          require_once 'include/db.php';
          $dbhost = $GLOBALS['dbhost'];
          $dbuser = $GLOBALS['dbuser'];
          $dbpass = $GLOBALS['dbpass'];
          $dbname = $GLOBALS['dbname'];
          $mysql = mysqli_connect("$dbhost","$dbuser","$dbpass","$dbname");

          if (!$mysql) {echo '<div class="alert alert-danger container" role="alert">Отсутствуеь подключение к базе данных<br>'. mysqli_connect_error() .PHP_EOL.'</div>';exit();}

          $mysql->set_charset("utf8");
          if ($sql = mysqli_query ($mysql, "SELECT * FROM `applications` ORDER BY `id` desc")){
              while ($result = mysqli_fetch_array($sql)) {
                
                      // ------------------------ Request ----------------------
                        
                         echo '<!-------applications------->
                        <div class="container-fluid">
                              <div class="accordion" id="accordionExample">
                              <div class="card "> 
                        <div class="card-head" id="heading'.$result['id'].'">
                                <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse'.$result['id'].'" aria-expanded="true" aria-controls="collapse'.$result['id'].'" style="width: 100%;">
                                  <table class="table table-hover table-bordered text-left '.$date_class.'" >
                                    <tr>';
                      if ($_SESSION['user_id']==1){echo '<td>id'.$result['id'].'</td>';}
                                echo '<td>Заявка № '.$result['app'].' / '.$result['app_old'].'</td>
                                      <td>Просимое время с '.date('d.m.Y H:i',$result['start_t']).'</td>
                                      <td>до '.date('d.m.Y H:i',$result['end_t']).'</td>
                                      <td>Заявку составил:'.$result['fullname'].'</td>
                                      <td>Закрыл заявку: '.$result['closed_the_app'].'</td>
                                    </tr>           
                                  </table>
                                </button>
                              </div>
                              <!---------------------   Body Request   ------------------>
                              <div id="collapse'.$result['id'].'" class="collapse" aria-labelledby="heading'.$result['id'].'" data-parent="#accordionExample">
                                <div class="card-body" style="padding: 5px 12px 5px 12px;">
                                  <table class="table table-bordered">
                                    <tbody>
                                      <tr>
                                        <td>Заявка № '.$result['app'].' / '.$result['app_old'].'</td>
                                        <td>Предприятие: '.$result['ent'].'</td>
                                        <td>Категория: '.$result['cat'].'</td>
                                        <td>Просимое время: c '.date('d.m.Y H:i',$result['start_t']).'</td>
                                        <td>до '.date('d.m.Y H:i',$result['end_t']).'</td>
                                        <td>Заявку составил: '.$result['fullname'].'</td>
                                        <td>Заявку продлил: '.$result['edit_user_app'].'</td>
                                      </tr>
                                      <tr>
                                        <td colspan="">Содержание работ: '.$result['job_cont'].'</td>
                                        <td colspan="">Оперативные указания: '.$result['op_guid'].'</td>
                                        <td colspan="">Остаются в работе: '.$result['rem_work'].'</td>
                                        <td colspan="">Выводятся из работы'.$result['der_work'].'</td>
                                        <td>Состояние: '.$result['status_appl'].'</td>
                                        <td>Дата продления: '.date('d.m.Y H:i',$result['date_edit']).'</td>
                                        <td>Дата закрытия: '.date('d.m.Y H:i',$result['date_close_app']).'</td>

                                      </tr>
                                    </tbody>
                                  </table>
                                  <div class="container text-center" style="margin:10px auto 10px auto;">';
                                  if ($_SESSION['user_id']==1){echo ' <button class="btn btn-danger" href="include/edit_request.php?id='.$result['id'].'" data-toggle="modal" data-target="#EditexampleModal'.$result['id'].'">Редактировать</button> ';}
                                    echo '<button class="btn btn-success" href="include/ext_request.php?id='.$result['id'].'" data-toggle="modal" data-target="#exampleModal'.$result['id'].'">Продлить</button> 
                                    <button class="btn btn-danger" ><a href="include/closed_request.php?id='.$result['id'].'" style="color:#fff">Закрыть</a></button>
                                      <form action="include/ext_request.php" method="get">
                                        <div class="modal fade text-left" id="exampleModal'.$result['id'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static">
                                          <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Продлить заявку</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                                <label >ID заявки</label>
                                                <input type="text" class="form-control" name="id" id="id" placeholder="" value="'.$result['id'].'" readonly>
                                                <label> Новый № Заявки</label>
                                                <input type="text" class="form-control" name="app" id="app" placeholder="" required></input>
                                                <label >№ Заявки</label>
                                                <input type="text" class="form-control" name="app_old" id="app_old" placeholder="" value="'.$result['app'].'"" readonly></input>
                                                <label >Предприятие:</label>
                                                <input type="text" class="form-control" name="ent" id="ent" placeholder="" value="'.$result['ent'].'" readonly>
                                                <label >Категория:</label>
                                                <input type="text" class="form-control" name="cat" id="cat" placeholder="" value="'.$result['cat'].'" readonly>
                                                <label >Содержание работ:</label>
                                                <input type="text" class="form-control" name="job_cont" id="job_cont" placeholder="" value="'.$result['job_cont'].'"; readonly>
                                                <label >Оперативные указания:</label>
                                                <input type="text" class="form-control" name="op_guid" id="op_guid" placeholder="" value="'.$result['op_guid'].'"; readonly>
                                                <label >Остаются в работе:</label>
                                                <input type="text" class="form-control" name="rem_work" id="rem_work" placeholder="" value="'.$result['rem_work'].'"; readonly>
                                                <label >Выводятся из работы:</label>
                                                <input type="text" class="form-control" name="der_work" id="der_work" placeholder="" value="'.$result['der_work'].'"; readonly>
                                                <label >Дата начала</label>
                                                <input type="text" name="start_t" class="datepicker-here form-control"  data-time-format=\'hh:ii\' data-position=\'top left\' placeholder="'.date('d.m.Y H:i',$result['start_t']).'" required></input>
                                                <label >Дата окончания</label>
                                                <input type="text" name="end_t" class="datepicker-here form-control"  data-time-format=\'hh:ii\' data-position=\'top left\' placeholder="'.date('d.m.Y H:i',$result['end_t']).'" required></input>
                                                <label >Заявку продлил:</label>
                                                <input type="text" class="form-control" name="fullname" id="fullname" placeholder="" value="'.$result['fullname'].'"; readonly>
                                                  <div class="modal-footer">
                                                    <button class="btn btn-success" type="submit" >Продлить</button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Выход</button>
                                                  </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </form>
                                      <form action="include/edit_request.php" method="get">
                                        <div class="modal fade text-left" id="EditexampleModal'.$result['id'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static">
                                          <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="EditexampleModalLongTitle">Продлить заявку</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                                <label >ID заявки</label>
                                                <input type="text" class="form-control" name="id" id="id" placeholder="" value="'.$result['id'].'" readonly>
                                                <label>№ Заявки</label>
                                                <input type="text" class="form-control" name="app" id="app" placeholder="" value="'.$result['app'].'"></input>
                                                <label>№ Старой заявки</label>
                                                <input type="text" class="form-control" name="app_old" id="app_old" placeholder="" value="'.$result['app_old'].'"></input>
                                                <label >Предприятие:</label>
                                                <select class="custom-select mb-3" id="ent" name="ent" onchange="ent();">
                                                  <option  value="Ставропольэнерго">Ставропольэнерго</option>
                                                  <option  value="ЗЭС">ЗЭС</option>
                                                  <option  value="ВЭС">ВЭС</option>
                                                  <option  value="НЭС">НЭС</option>
                                                  <option  value="ПЭС">ПЭС</option>
                                                  <option  value="ЦЭС">ЦЭС</option>
                                                  <option  value="ЗЭС">СЭС</option>
                                                </select>
                                                <label >Категория</label>
                                                <select class="custom-select mb-3" id="cat" name="cat" onchange="cat();">
                                                  <option  value="НПЛ">НПЛ</option>
                                                  <option  value="ПЛ">ПЛ</option>
                                                  <option  value="АВ">АВ</option>
                                                  <option  value="НО">НО</option>
                                                </select>
                                                <label >Содержание работ:</label>
                                                <input type="text" class="form-control" name="job_cont" id="job_cont" placeholder="" value="'.$result['job_cont'].'";>
                                                <label >Оперативные указания:</label>
                                                <input type="text" class="form-control" name="op_guid" id="op_guid" placeholder="" value="'.$result['op_guid'].'"; >
                                                <label >Остаются в работе:</label>
                                                <input type="text" class="form-control" name="rem_work" id="rem_work" placeholder="" value="'.$result['rem_work'].'";>
                                                <label >Выводятся из работы:</label>
                                                <input type="text" class="form-control" name="der_work" id="der_work" placeholder="" value="'.$result['der_work'].'";>
                                                <label >Дата начала</label>
                                                <input type="text" name="start_t" class="datepicker-here form-control"  data-time-format=\'hh:ii\' data-position=\'top left\' placeholder="'.date('d.m.Y H:i',$result['start_t']).'" value="'.$result['start_t'].'" required></input>
                                                <label >Дата окончания</label>
                                                <input type="text" name="end_t" class="datepicker-here form-control"  data-time-format=\'hh:ii\' data-position=\'top left\' placeholder="'.date('d.m.Y H:i',$result['end_t']).'" required></input>
                                                <label >Статус заявки:</label>
                                                <input type="text" class="form-control" name="status_appl" id="status_appl" placeholder="" value="'.$result['status_appl'].'";>
                                                <label >Заявку создал (login):</label>
                                                <input type="text" class="form-control" name="respon" id="respon" placeholder="" value="'.$result['respon'].'";>
                                                <label >Заявку создал:</label>
                                                <input type="text" class="form-control" name="fullname" id="fullname" placeholder="" value="'.$result['fullname'].'";>
                                                  <div class="modal-footer">
                                                    <button class="btn btn-success" type="submit" >Сохранить</button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Выход</button>
                                                  </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </form>
                                      <!------------------------->
                                  </div>
                                </div>
                              </div>
                              </div><!--card-->
                              </div><!--accordion-->
                              </div><!--container-fluid-->';
              
            }
          }elseif (isset($sql)){echo '<div class="alert alert-danger container" role="alert">Отсутствует база данных / указана неверно</div>';}
          $mysql->close();?>
   
<!-----------------script---------------------->
<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js" ></script>
<script src="js/bootstrap.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<!--<script src="js/bootstrap-datepicker.ru.min.js"></script>-->
<script src="js/datepicker.js"></script>
<script>
  // Выбор сегодняшнего дня
$('.datepicker-here').datepicker({
    todayButton: new Date(),
    minutesStep: 10,
    timepicker: true

})

</script>
</body>
</html>