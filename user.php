<?php 
session_start(); 
if ($_SESSION['user_id'] != '1'){
	header('Location: /index.php');
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/page.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>СКиУЗ</title>
</head>
<body>
  
<?php 
include 'include/head.html';
?>


	<br><h1 class="text-center">Список пользователей</h1><br>
<?php error_reporting(0); 
require_once 'include/db.php';
$dbhost = $GLOBALS['dbhost'];
$dbuser = $GLOBALS['dbuser'];
$dbpass = $GLOBALS['dbpass'];
$dbname = $GLOBALS['dbname'];

  $mysql = mysqli_connect("$dbhost","$dbuser","$dbpass","$dbname");
  
    if (!$mysql) {echo '<div class="alert alert-danger container" role="alert">Отсутствуеь подключение к базе данных<br>'. mysqli_connect_error() .PHP_EOL.'</div>';}


    $mysql->set_charset("utf8");
  if ($sql = mysqli_query ($mysql, "SELECT * FROM `users` ORDER BY `id`")){
    echo 
    '<div class="container text-center">
      <table class="table table-hover" style="margin-bottom:15px;">
        <tbody style="font-weight: 400;">
          <tr>
            <th scope="col">id</td>
            <th scope="col">Логин</td>
            <th scope="col">Имя пользователя</td>
            <th scope="col">Дата регистрации</td>
            <th scope="col">Привелегии</td>
            <th scope="col" colspan="2">Редактировать</td>
          </tr>
        </tbody>';
    while ($result = mysqli_fetch_array($sql)) {
      echo'
        <tbody>
          <tr>
          <td>'.$result['id'].'</td>
          <td>'.$result['login'].'</td>
          <td>'.$result['fullname'].'</td>
          <td>Число: '.date('d.m.Y', $result['date']).' время: '.date('H:i:s', $result['date']).'</td>'          
            ;if ($result['privilege']==1) {
              echo '<td>Администратор</td>';
            }else {
              echo '<td>Пользователь</td>';
            } 
            echo '
            <td >
            <a href="include/useredit.php?id='.$result['id'].'" data-toggle="modal" data-target="#exampleModal'.$result['id'].'"><img src="img/edit.svg" width="20" height="20" class="butbase" data-toggle="tooltip" title="Редактирование пользователя &quot;'.$result['fullname'].'&quot;"></a>
            </td>

            <form action="include/useredit.php" method="get">
              <div class="modal fade text-left" id="exampleModal'.$result['id'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Редактировать пользователя</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <label >ID пользователя</label>
                          <input type="text" class="form-control" name="id" id="id" placeholder="" value="'.$result['id'].'" readonly>
                        <label >Логин пользователя</label>
                          <input type="text" class="form-control" name="login" id="login" placeholder="" value="'.$result['login'].'"></input>
                        <label >Пароль</label>
                          <input type="text" class="form-control" name="pass" id="pass" placeholder="">
                        <label >Имя пользователя</label>
                          <input type="text" class="form-control" name="fullname" id="fullname" placeholder="" value="'.$result['fullname'].'">
                          <label >Дата регистрации</label>
                          <input type="text" class="form-control" name="" id="" placeholder="'.date('d.m.Y H:i', $result['date']).'" disabled>
                        <label >Привелегии пользователя</label>
                          <select class="custom-select mb-3" id="myCheckbox" name="myCheckbox" onchange="myCheckbox();">
                            <option  value="0">Пользователь</option>
                            <option  value="1">Администратор</option>
                        </select>
                        <div class="modal-footer">
                            <button class="btn btn-success" type="submit" >Изменить</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Выход</button>
                        </div>
                  </div>
                </div>
              </div>
            </form>
            <form method="get" action="userdell.php">
              <td>
                <a href="include/userdell.php?id='.$result['id'].'"><img src="img/delete.svg" width="20" height="20" class="butbase" data-toggle="tooltip" title="Удалить пользователя &quot;'.$result['fullname'].'&quot;"></a>
              </td>
            </form>
        </tbody>';
}
echo '</table></div>';
}elseif (isset($sql)){echo '<div class="alert alert-danger container" role="alert">Отсутствует база данных / указана неверно</div>';}




  $mysql->close();
   ?>


<div class="container text-center">
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Добавить пользователя
</button>

<!-- Modal -->
<div class="modal fade text-left" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Добавить пользователя</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    
<form action="include/useradd.php" method="post" enctype="multipart/form-data">
  <label >Логин пользователя</label>
  <input type="text" class="form-control" name="login" id="login" placeholder="" required>
  <label >Фамилия ОИ</label>
  <input type="text" class="form-control" name="name" id="name" placeholder="" required>
  <label >Пароль пользователя</label>
  <input type="text" class="form-control" name="pass" id="pass" placeholder="" required>
    <label >Привелегии пользователя</label>
      <select class="custom-select mb-3" id="myCheckbox" name="myCheckbox" onchange="myCheckbox();">
      <option  value="0">Пользователь</option>
      <option  value="1">Администратор</option>
    </select>




  





      
      <div class="modal-footer">
                <button class="btn btn-success" type="submit">Регистрация</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Выход</button>

      </div>
      </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
	<script src="js/jquery-3.3.1.slim.min.js"></script>
	<script src="js/popper.min.js" ></script>
	<script src="js/bootstrap.js"></script>
  <script src="js/bootstrap-modal.js"></script>
<script>
    $(function () {
        // инициализация всплывающих подсказок
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
</body>
</html>